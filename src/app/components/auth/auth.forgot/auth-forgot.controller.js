function AuthForgotController(AuthService, $state) {
    const $ctrl = this;

    $ctrl.$onInit = function () {
        $ctrl.closeModal();
    }

    $ctrl.closeModal = function () {
        var modal = document.getElementById("reset-modal");
        modal.style.display = "none";
    }

    $ctrl.submitForm = function () {
        return AuthService
            .newPassword($ctrl.email)
            .then(function () {
                alert("Check your email to reset your password!");
                $ctrl.closeModal();
            }, function (reason) {
                alert("Incorrect password! Try again");
                $ctrl.error = reason.message;
            });
    }
}

angular
    .module('components.auth')
    .controller('AuthForgotController', AuthForgotController);
