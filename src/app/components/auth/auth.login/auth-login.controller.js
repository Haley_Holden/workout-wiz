function LoginController(AuthService, $state) {
    const $ctrl = this;

    $ctrl.$onInit = function () {
        $ctrl.error = null;
        $ctrl.user = {
            email: '',
            password: ''
        }
    }

    $ctrl.showResetModal = function (event) {
        document.getElementById("reset-modal").style.display = "block";
        document.getElementById("close").style.display = "block";
    }

    $ctrl.loginUser = function (event) {
        return AuthService
            .login(event.user)
            .then(function () {
                // fully authenticated
                $state.go('home');
            }, function (reason) {
                alert("Incorrect password! Try again");
                $ctrl.error = reason.message;
            });
    }
}

angular
    .module('components.auth')
    .controller('LoginController', LoginController);
