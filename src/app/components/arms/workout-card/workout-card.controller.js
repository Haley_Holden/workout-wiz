function WorkoutCardController($scope) {
    const $ctrl = this;

    // display all when the workout card is clicked and reset the timer
    $ctrl.showModal = function (workout) {
        $ctrl.workoutSelected = workout;
        document.getElementById("workout-modal").style.display = "block";
        document.getElementById("close").style.display = "block";
        document.getElementById("timer").style.display = "block";
        document.getElementById("start-button").style.display = "block";
        document.getElementById("resume-button").style.display = "block";
        document.getElementById("stop-button").style.display = "block";
        $scope.$broadcast('timer-start');
        $scope.$broadcast('timer-stop');
    }
}

angular.module("components.arms").controller("WorkoutCardController", WorkoutCardController);
