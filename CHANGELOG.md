0.4.0 / 2020-11-12
------------------

- Utilized angular-timer npm package to add ability to start/stop individual workouts
- Created Tracker Model to keep track of workouts individual users have started on their account
- Fixed navbar to work in angular architecture
- Fixed Create component's dropdowns to update input dynamically
- Added get requests to Parse services to display all workout data
- Added ability to upload and display images for each exercise
- Added logout and reset password functionality 

0.3.0 / 2020-10-14
------------------

- Configured authentication using Parse User Model. 
- Restricted access to protected routes based on authentication. 

0.2.0 / 2020-09-30
------------------

- Implemented gulp build system.
- Organized file system into appropriate structure.
- Added Routing to Components.
- Created Parse Models and connected to data in Back4App.


0.1.0 / 2016-09-16
------------------

- Developed multiple components with data and event binding.
- Created custom services.
