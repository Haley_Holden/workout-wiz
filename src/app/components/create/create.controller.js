function CreateController(WorkoutModel, CircuitModel, ExerciseModel, $state) {

    const $ctrl = this;


    $ctrl.$onInit = function () { // Initialize values for create workout form
        $ctrl.num_circuits = 2;
        $ctrl.num_exercises = 4;
    }

    $ctrl.updateWorkout = function () {

        // Add workout to Parse database
        return WorkoutModel.addWorkout(this.workout)
            .then((workoutID) => {
                var circuitList = [] // Array of promises for circuit put requests
                for (const [key, exerciseObj] of Object.entries(this.workout.circuits)) {
                    circuitList.push(CircuitModel.addCircuit(workoutID));
                }
                return circuitList;
            })
            .then((circuitList) => {
                return Promise.all(circuitList); // Ensure circuits finish uploading
            })
            .then((circuitList) => {
                // Loop through circuits
                for (const [circuitKey, exerciseObj] of Object.entries(this.workout.circuits)) {

                    var circuitID = circuitList[circuitKey];

                    // Loop through exercises in each circuit
                    for (const [key, exercise] of Object.entries(exerciseObj)) {
                        var exList = []

                        // Associate each picture with each exercise
                        var pictureInfo = document.getElementById("picture" + circuitKey + "." + key);
                        if (pictureInfo.files.length > 0) {
                            var picture = pictureInfo.files[0];
                        }
                        exList.push(ExerciseModel.addExercise(circuitID, exercise, picture));
                    }
                }
                return exList
            }).then((exList) => {
                return Promise.all(exList); // Ensure exercises are fully loaded before exiting

            }).then((result) => {
                return $state.go('arms'); // Navigate to grid of workouts
            })
            .catch(error => Promise.reject(error));

    }


    $ctrl.types = ['Arms', 'Cardio', 'Core', 'Legs']; // Setting options for create workout form
    $ctrl.timeOn = ['30', '40', '45', '60'];
    $ctrl.timeOff = ['5', '10', '15'];
    $ctrl.range = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    $ctrl.default = 3;
    $ctrl.exercises = '3';

}

angular.module("components.create").controller("CreateController", CreateController);
