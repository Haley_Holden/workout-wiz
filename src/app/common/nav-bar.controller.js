function NavBarController(AuthService, $state) {
    const $ctrl = this;

    // Utilize Parse.User logout function to delete the current user data from local storage
    $ctrl.logOut = function () {
        AuthService.logout().then((result) => {
            $state.go('auth.login');
        }).catch(error => {
            console.log(error);
        })
    }

}

angular.module("common").controller("NavBarController", NavBarController);
