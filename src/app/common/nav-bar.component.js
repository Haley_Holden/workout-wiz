const navBar = {
    templateUrl: './nav-bar.html',
    controller: 'NavBarController'
}

// NavBar Component with Routing (Routed / Stateful)
angular.module('common').component('navBar', navBar);