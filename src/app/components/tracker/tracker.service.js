// Interacts with Parse User database. 
function TrackerService () {
    var user = Parse.User;

    this.isAuthenticated = function () {
        return !!(user.current() && user.current().authenticated());
    };

    this.getUser = function () {
        return user
            .current();
    };
}

angular
    .module('components.tracker')
    .service('TrackerService', TrackerService);
