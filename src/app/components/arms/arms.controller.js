function ArmsController(AuthService, $state) {
    const $ctrl = this;
    $ctrl.workoutInfo = {}


    /****************************************
     * Create workout structure with workouts 
     * objects accessible by id. For each 
     * workout, add an attribute 'circuits'
     * that holds an array of exercises for
     * that circuit for the correct workout
    *************************************/
    $ctrl.$onInit = function () {
        for (var workout of $ctrl.workouts) {
            $ctrl.workoutInfo[workout.id] = workout;
            $ctrl.workoutInfo[workout.id]["circuits"] = [];
            for (var circuit of $ctrl.circuits) {
                if (circuit.workout.id == workout.id) {
                    var circ = [];
                    for (var exercise of $ctrl.exercises) {
                        if (exercise.circuit.id == circuit.id) {
                            circ.push(exercise);
                        }
                    }
                    $ctrl.workoutInfo[workout.id]["circuits"].push(circ);
                }
            }
        }
    }
}



angular
    .module("components.arms")
    .controller("ArmsController", ArmsController);
