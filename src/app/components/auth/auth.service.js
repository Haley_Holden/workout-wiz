// Interacts with Parse User database. 

function AuthService() {
    var auth = Parse.User;
    var authData = null;

    function storeAuthData(response) {
        authData = response;
        return authData;
    }

    function clearAuthData() {
        authData = null;
    }
    this.login = function (user) {
        return auth
            .logIn(user.email, user.password)
            .then(storeAuthData);
    };
    this.register = function (user) {
        return auth
            .signUp(user.email, user.password)
            .then(storeAuthData);
    };
    this.logout = function () {
        return auth
            .logOut()
            .then(clearAuthData);
    };

    this.isAuthenticated = function () {
        return !!(auth.current() && auth.current().authenticated());
    };

    this.getUser = function () {
        return auth
            .current();
    };

    this.newPassword = function (email) {
        return auth.requestPasswordReset(email);
    }


}

angular
    .module('components.auth')
    .service('AuthService', AuthService);
