class CircuitModel {
    constructor(Parse) {
        this.Parse = Parse;
        this.name = "circuit";
        this.fields = [
            "workout"
        ];
        this.data = {}; //hold singualr result of queries
        this.collection = []; // hold array results of queries
    }

    New(obj) {
        // Creating a new circuit Parse Object
        if (angular.isUndefined(obj)) {
            const parseObject = new this.Parse.Object(this.name);
            this.Parse.defineAttributes(parseObject, this.fields);
            return parseObject;
        } else {

            this.Parse.defineAttributes(obj, this.fields);
            return obj;
        }
    }

    // Push circuit to the database
    addCircuit(workoutID) {
        const circuit = Parse.Object.extend("circuit");
        const data = new circuit();
        data.set("workout", workoutID);

        return data.save()
            .then((data) => {
                return data;
            }, (error) => {
                alert('Failed to create circuit, with error code: ' + error.message);
            });
    }


    // Get an array of all circuit objects in the database
    getAllCircuits() {
        return new this.Parse.Query(this.New())
            .find(results => {
                results.forEach(result => {
                    this.Parse.defineAttributes(result, this.fields);
                })
                return Promise.resolve(results);
            })
            .catch(error => Promise.reject(error));
    }
}

angular
    .module('common')
    .service('CircuitModel', CircuitModel);
