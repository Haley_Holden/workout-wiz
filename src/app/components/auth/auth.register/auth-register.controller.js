function RegisterController(AuthService, $state, TrackerModel) {
    const $ctrl = this;

    $ctrl.$onInit = function () {
        $ctrl.error = null;
        $ctrl.user = {
            email: '',
            password: ''
        }
    }

    $ctrl.createUser = function (event) {
        return AuthService
            .register(event.user)
            .then(function (newUser) {
                // fully authenticated
                TrackerModel.newUserTracking(newUser);
                $state.go('app');
            }, function (reason) {
                $ctrl.error = reason.message;
            });
    };
}

angular
    .module('components.auth')
    .controller('RegisterController', RegisterController);
