class ExerciseModel {
    constructor(Parse) {
        this.Parse = Parse;
        this.name = "exercise";
        this.fields = [
            "name",
            "circuit"
        ];
        this.data = {}; //hold singualr result of queries
        this.collection = []; // hold array results of queries
    }

    New(obj) {
        // Creating a new exercise Parse Object
        if (angular.isUndefined(obj)) {
            const parseObject = new this.Parse.Object(this.name);
            this.Parse.defineAttributes(parseObject, this.fields);
            return parseObject;
        } else {
            // Exposing Lesson Parse Object attributes (getters and setter)
            this.Parse.defineAttributes(obj, this.fields);
            return obj;
        }
    }


    // Push exercise with image to the database
    addExercise(circuitID, exerciseData, picture) {
        const exercise = Parse.Object.extend("exercise");
        const data = new exercise();

        data.set("name", exerciseData);
        data.set("circuit", circuitID);

        const parseFile = new Parse.File(exerciseData, picture);
        return parseFile.save()
            .then((parseFile) => {
                data.set("picture", parseFile);
                return data.save()
                    .then((data) => {
                        return Promise.resolve(data.id);
                    }, (error) => {
                        alert('Failed to save image with error code: ' + error.message);
                    })
            }, (error) => {
                alert('Failed to create exercise, with error code: ' + error.message);
            })



    }

    // Get an array of all exercise objects in the database
    getAllExercises() {
        return new this.Parse.Query(this.New())
            .find(results => {
                results.forEach(result => {
                    this.Parse.defineAttributes(result, this.fields);
                })
                return Promise.resolve(results);
            })
            .catch(error => Promise.reject(error));
    }
}

angular
    .module('common')
    .service('ExerciseModel', ExerciseModel);
