const home = {
    templateUrl: "./home.html",
    controller: "HomeController",
};

// Arms Component with Routing (Routed / Stateful)
angular
    .module("components.home")
    .component("home", home)
    .config(function ($stateProvider) {
        $stateProvider
            .state('home', {
                parent: 'app',
                url: '/home',
                component: 'home'
            });
    });
