class TrackerModel {
    constructor(Parse) {
        this.Parse = Parse;
        this.name = "tracker";
        this.fields = [
            "user",
            "trackedWorkouts"
        ];
        this.data = {}; //hold singualr result of queries
        this.collection = []; // hold array results of queries
    }

    New(obj) {
        // Creating a new tracker Parse Object
        if (angular.isUndefined(obj)) {
            const parseObject = new this.Parse.Object(this.name);
            this.Parse.defineAttributes(parseObject, this.fields);
            return parseObject;
        } else {
            // Exposing Lesson Parse Object attributes (getters and setter)
            this.Parse.defineAttributes(obj, this.fields);
            return obj;
        }
    }

    /***********************************************
     * When a new user is added, a new tracking 
     * object is created in the database with a 
     * pointer to the new user, and an empty array 
     * of the workouts being tracked
     ***********************************************/
    newUserTracking(user) {
        const Tracker = Parse.Object.extend("tracker");
        const object = new Tracker();
        object.set("user", user);
        object.set("trackedWorkouts", [])

        return object
            .save()
            .catch(error => Promise.reject(error));
    }

    /***********************************************
     * When a user begins a workout by starting the 
     * timer, that workout is added to the current 
     * user's tracking object by being appended to 
     * the array attribute 'trackedWorkouts'
     ***********************************************/
    addTracking(user, workoutId) {
        const Tracker = Parse.Object.extend("tracker");
        const query = new Parse.Query(Tracker);
        return query
            .equalTo("user", user)
            .first(object => {
                var currArr = object.attributes.trackedWorkouts;
                if(!currArr.includes(workoutId)){
                    currArr.push(workoutId);
                }
                object.set("trackedWorkouts", currArr);
                object.save();
            })
            .catch(error => Promise.reject(error));
    }

    /***********************************************
     * Gets the array of tracked workouts for a
     * specified user
     ***********************************************/
    getUserTracking(userObject) {
        return new this.Parse.Query(this.New())
            .equalTo("user", userObject)
            .find(results => {
                results.forEach(result => {
                    this.Parse.defineAttributes(result, this.fields);
                })
                return Promise.resolve(results);
            })
            .catch(error => Promise.reject(error));
    }
}

angular
    .module('common')
    .service('TrackerModel', TrackerModel);
