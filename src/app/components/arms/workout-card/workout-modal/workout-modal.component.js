const workoutModal = {
    templateUrl: './workout-modal.html',
    controller: 'WorkoutModalController',
    bindings: {
        selectedWorkout: '<'
    }
}

// Workout Modal component which displays details of exercise with routing
angular.module('components.arms').component('workoutModal', workoutModal)
