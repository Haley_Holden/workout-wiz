const create = {
    templateUrl: './create.html',
    controller: 'CreateController',
    bindings: {
        createCircuits: '&',
        createExercises: '&',
        workout: '<',
        onUpdate: '&'
    }
};

// Create Component with Routing (Routed / Stateful)
angular
    .module('components.create')
    .filter('range', function() {
        return function(input, min, max) {
            min = parseInt(min); //Make string input int
            max = parseInt(max);
            for (var i=min; i<max; i++)
            input.push(i);
            return input;
        };
    })
    .component('create', create)
    .config(function ($stateProvider) {
        $stateProvider
            .state('create', {
                parent: 'app',
                url: '/create',
                component: 'create'
            });
    });
