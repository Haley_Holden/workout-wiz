function WorkoutModalController($scope, TrackerModel, TrackerService) {
    const $ctrl = this;
    $ctrl.user = TrackerService.getUser();

    // Initially have the modal closed
    $ctrl.$onInit = function () {
        $ctrl.closeModal();
    }

    // add a workout to the current user's tracked workouts
    $ctrl.trackWorkout = function (user, id) {
        return TrackerModel.addTracking(user, id);
    }

    // use the angular-timer call to start the timer and track the workout
    $ctrl.startTimer = function (){
        $scope.$broadcast('timer-start');
        $ctrl.timerRunning = true;
        $ctrl.disableResume = true;
        $ctrl.trackWorkout($ctrl.user, $ctrl.selectedWorkout.id);
    };

    // use the angular-timer call to resume the timer
    $ctrl.resumeTimer = function () {
        $scope.$broadcast('timer-resume');
        $ctrl.timerRunning = true;
        $ctrl.disableResume = true;

    };

    // use the angular-timer call to pause the timer
    $ctrl.stopTimer = function () {
        $scope.$broadcast('timer-stop');
        $ctrl.timerRunning = false;
        $ctrl.disableResume = false;
    };
 
 
    // Hides modal div when x button is clicked
    // stops the timer
    $ctrl.closeModal = function () {
        var modal = document.getElementById("workout-modal");
        modal.style.display = "none";
        $scope.$broadcast('timer-stop');
        $ctrl.timerRunning = false;
        $ctrl.disableResume = true;
    }

}

angular.module("components.arms").controller("WorkoutModalController", WorkoutModalController);
