const tracker = {
    templateUrl: "./tracker.html",
    controller: "TrackerController",
    bindings: {
        workouts: '<',
        circuits: '<',
        exercises: '<',
        tracker: '<'
    }
};

// Tracker Component with Routing (Routed / Stateful)
angular
    .module("components.tracker")
    .component("tracker", tracker)
    .config(function ($stateProvider) {
        $stateProvider
            .state('tracker', {
                parent: 'app',
                url: '/tracker',
                component: 'tracker',
                resolve: {
                    workouts: function (WorkoutModel) {
                        return WorkoutModel.getAllWorkouts()
                    },
                    circuits: function (CircuitModel) {
                        return CircuitModel.getAllCircuits()
                    },
                    exercises: function (ExerciseModel) {
                        return ExerciseModel.getAllExercises()
                    },
                    tracker: function (TrackerService, TrackerModel) {
                        var user = TrackerService.getUser();
                        return TrackerModel.getUserTracking(user);
                    }
                }
            });
    });
