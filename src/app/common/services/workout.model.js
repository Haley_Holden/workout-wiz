class WorkoutModel {
    constructor(Parse) {
        this.Parse = Parse;
        this.name = "workout";
        this.fields = [
            "name",
            "type",
            "timeOn",
            "timeOff",
            "reps"
        ];
        this.data = {}; //hold singualr result of queries
        this.collection = []; // hold array results of queries
    }

    New(obj) {
        // Creating a new Workout Parse Object
        if (angular.isUndefined(obj)) {
            const parseObject = new this.Parse.Object(this.name);
            this.Parse.defineAttributes(parseObject, this.fields);
            return parseObject;
        } else {
            // Exposing Lesson Parse Object attributes (getters and setter)
            this.Parse.defineAttributes(obj, this.fields);
            return obj;
        }
    }

    // Push workout to the database
    addWorkout(workoutData) {
        const workout = Parse.Object.extend("workout");
        const data = new workout();
        data.set("name", workoutData.name);
        data.set("timeOn", parseInt(workoutData.timeOn));
        data.set("timeOff", parseInt(workoutData.timeOff));
        data.set("type", workoutData.type);

        return data.save()
            .then((data) => {
                return Promise.resolve(data);
            }, (error) => {
                alert('Failed to create workout, with error code: ' + error.message);
            });
    }



    // Get an array of all workout objects in the database
    getAllWorkouts() {
        return new this.Parse.Query(this.New())
            .find(results => {
                results.forEach(result => {
                    this.Parse.defineAttributes(result, this.fields);
                })
                return Promise.resolve(results);
            })
            .catch(error => Promise.reject(error));
    }
}

angular
    .module('common')
    .service('WorkoutModel', WorkoutModel);
