const authForgot = {
    templateUrl: './auth-forgot.html',
    controller: 'AuthForgotController'
}

angular
    .module('components.auth')
    .component('authForgot', authForgot)
