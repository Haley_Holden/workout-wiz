const arms = {
    templateUrl: "./arms.html",
    controller: "ArmsController",
    bindings: {
        workouts: '<',
        circuits: '<',
        exercises: '<'
    }
};

// Arms Component with Routing (Routed / Stateful)
angular
    .module("components.arms")
    .component("arms", arms)
    .config(function ($stateProvider) {
        $stateProvider
            .state('arms', {
                parent: 'app',
                url: '/arms',
                component: 'arms',
                resolve: {
                    workouts: function (WorkoutModel) {
                        return WorkoutModel.getAllWorkouts()
                    },
                    circuits: function (CircuitModel) {
                        return CircuitModel.getAllCircuits()
                    },
                    exercises: function (ExerciseModel) {
                        return ExerciseModel.getAllExercises()
                    },
                }
            });
    });
