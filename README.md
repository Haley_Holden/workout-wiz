**Workouts for Wizards**
--------------------------------------

**_Setup_**

Run the following command(s) to install the necessary extra packages:

- npm install

**_Running the Program_**

- Run _gulp_ in the command line
- Navigate to http://0.0.0.0:8885

**_Navigating the Program_**

* Users not logged in will be prompted to log in or register as a new users
    * There is also an option to reset a user's password if forgottens  
- Once logged in, users will be brought to the home page where they have the option to log out or navigate to
    - Workouts
    - Create
    - Tracker
* Workouts 
    * Users can view a grid of workout cards compiled from the full database of workouts submitted by all users
    * Workout cards display the workout name, workout type, number of circuits and exercises, time on exercise, and time resting between each exercise
    * When a workout card is clicked, a modal appears with a timer at the top
        - A user can start the timer to begin a workout
        - When the timer is stopped, the user can either resume the timer later or restart
        - The individual exercises per circuit are displayed with corresponding images if applicable
- Create
    - Users can create a workout by filling out the same type of information found on the pre-existing workout cards
    - Users can change the number of circuits and exercises they can input by changing the dropdown values
    - Images can be uploaded to go along with the exercises
* Tracker
    * If a user selects a workout card on the Workouts page and starts the timer to start the workout, that workout will be added to that User's tracking page