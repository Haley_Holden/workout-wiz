const workoutCard = {
        templateUrl: './workout-card.html',
        controller: 'WorkoutCardController',
        bindings: {
            workoutInfo: '<'
        }
    }
    
// Workout Card Component with Routing (Routed / Stateful)
angular
    .module('components.arms')
    .component('workoutCard', workoutCard);