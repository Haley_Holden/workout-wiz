function AppController(AuthService, $state) {
    const $ctrl = this;
    $ctrl.user = AuthService.getUser();

}

angular.module("common")
    .controller("AppController", AppController);
