function TrackerController(TrackerService, TrackerModel) {
    const $ctrl = this;
    $ctrl.workoutInfo = {}

    $ctrl.user = TrackerService.getUser();
    $ctrl.userId = $ctrl.user.id;

    /*************************************************
     * Create workout structure with workouts objects 
     * accessible by id. For each workout, add an 
     * attribute 'circuits' that holds an array of 
     * exercises for that circuit for the correct 
     * workout. Only workouts identified by the 
     * current user's tracked workouts will be added
    **************************************************/
    $ctrl.$onInit = function () {
        var trackArr = $ctrl.tracker[0].trackedWorkouts;

        for (var workout of $ctrl.workouts) {
            if(trackArr.includes(workout.id)) {
                $ctrl.workoutInfo[workout.id] = workout;
                $ctrl.workoutInfo[workout.id]["circuits"] = [];
                for (var circuit of $ctrl.circuits) {
                    if (circuit.workout.id == workout.id) {
                        var circ = [];
                        for (var exercise of $ctrl.exercises) {
                            if (exercise.circuit.id == circuit.id) {
                                circ.push(exercise);
                            }
                        }
                        $ctrl.workoutInfo[workout.id]["circuits"].push(circ);
                    }
                }
            }
        }
    }
}



angular
    .module("components.tracker")
    .controller("TrackerController", TrackerController);
