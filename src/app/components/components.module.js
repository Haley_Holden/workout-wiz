angular
    .module('components', [
        'components.home',
        'components.arms',
        'components.create',
        'components.tracker',
        'components.auth'
    ]);
